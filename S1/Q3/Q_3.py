import numpy as np

people_number = int(input())
inp_list = []

for i in range(people_number):
    inp_list.append(input().split())
choices = np.array(inp_list)
Genres = ['Action', 'Adventure', 'Comedy', 'History', 'Horror', 'Romance']
G_nums = np.zeros((6, 2), dtype=int)
G_nums[:, 0] = np.array([0, 1, 2, 3, 4, 5])

for i in range(people_number):
    for j in range(1, 4):
        for k in range(6):
            if choices[i, j] == Genres[k]:
                G_nums[k, 1] += 1

G_nums = G_nums[G_nums[:, 1].argsort()][::-1]
G_nums_sorted = G_nums[G_nums[:, 1].argsort()][::-1]

for i in range(6):
    print(Genres[G_nums_sorted[i, 0]], ": {}".format(G_nums_sorted[i, 1]))
