import numpy as np

Games = []
for i in range(6):
    Games.append(input())
result = np.zeros((6, 2), dtype=int)
for i in range(6):
    result[i, 0] = int(Games[i][0])
    result[i, 1] = int(Games[i][2])
#print("here we go", result)

teams = ['Iran', 'Morocco', 'Portugal', 'Spain']
Plays = np.array([[0, 3], [0, 2], [0, 1], [3, 2], [3, 1], [2, 1]])
scores = np.zeros((4, 6), dtype=int)
scores[:, 5] = np.array([0, 1, 2, 3])


def point_giver(fs_game):
    rs_game = [0, 0]
    if fs_game[0] == max(fs_game):
        rs_game[0] = 3
    if fs_game[1] == max(fs_game):
        rs_game[1] = 3
    if rs_game[0] == rs_game[1]:
        rs_game[0], rs_game[1] = 1, 1
    return rs_game


def win_lose_draw(ps_game):
    rs_game = np.zeros((2, 3), dtype=int)
    if ps_game[0] > ps_game[1]:
        rs_game[0, 0] = 1
        rs_game[1, 1] = 1
    if ps_game[0] < ps_game[1]:
        rs_game[1, 0] = 1
        rs_game[0, 1] = 1
    if ps_game[0] == ps_game[1]:
        rs_game[:, 2] = 1, 1
    return rs_game


for i in range(6):
    scores[Plays[i, :], 4] += point_giver(result[i])
    scores[Plays[i, :], 0:3] += win_lose_draw(point_giver(result[i]))
    scores[Plays[i, :], 3] += result[i, 0] - \
        result[i, 1], result[i, 1] - result[i, 0]

score_wsorted = scores[scores[:, 0].argsort()][::-1]
score_psorted = score_wsorted[score_wsorted[:, -2].argsort()][::-1]

for i in range(4):
    print(teams[score_psorted[i, -1]],
          " wins:{} , loses:{} , draws:{} , goal difference:{} , points:{}".format(score_psorted[i, 0], score_psorted[i, 1], score_psorted[i, 2], score_psorted[i, 3], score_psorted[i, 4]))
