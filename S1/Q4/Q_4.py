people_number = int(input())
inp_list = []

for i in range(people_number):
    inp_list.append(input().split('.'))
    inp_list[i][1] = inp_list[i][1].capitalize()

sorted_list = sorted(inp_list)

for i in range(people_number):
    print(sorted_list[i][0], sorted_list[i][1], sorted_list[i][2])
