import numpy as np


def factors_checker(num):
    d_num = 2
    f_num = 0
    while d_num <= num:
        if num % d_num == 0:
            while num % d_num == 0:
                num /= d_num
            f_num += 1
        d_num += 1
    return f_num


numbers = np.zeros(10)
for i in range(10):
    numbers[i] = input()
factors = np.zeros(10)
for i in range(10):
    factors[i] = factors_checker(numbers[i])

check_list = []
for i in range(10):
    if factors[i] == max(factors):
        check_list.append((numbers[i],factors[i]))
check_list = np.array(check_list)
for i in range(len(check_list)):
    if check_list[i,0] == max(check_list[:,0]):
        print(int(check_list[i,0]),int(check_list[i,1]))
