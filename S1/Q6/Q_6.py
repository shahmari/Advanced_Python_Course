dict_number = int(input())
inp_list = []

for i in range(dict_number + 1):
    inp_list.append(input().split())

print_list = []
for i in range(len(inp_list[-1])):
    check = 0
    for x in range(dict_number):
        for y in range(1, 4):
            if inp_list[-1][i] == inp_list[x][y]:
                print_list.append(inp_list[x][0])
                check = 1
    if check == 0:
        print_list.append(inp_list[-1][i])

for i in print_list:
    print(i, end=' ')
